const jwt = require('jsonwebtoken');
const Secret_key = "SOMETHING";


const driverAuth = (req, res, next) => {
    try {
        let token = req.headers.authorization;
        // console.log(token);
        if (token) {
            token = token.split(" ")[1];
            let user = jwt.verify(token, Secret_key);
            // console.log(user.data[0].role);
            req.body = { userId: user.data[0].id, role: user.data[0].role };
        } else {
            return res.status(401).json({ message: "unauthorized user" })
        }
        next();
    } catch (error) {
        console.log(error);
        res.status(401).json({ message: "unauthorized user" });
    }
}

module.exports = driverAuth