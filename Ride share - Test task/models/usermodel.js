const mongoose = require('mongoose');

const userSchema = mongoose.Schema({
    name:{
        type: String,
        required : true,
    },
    password:{
        type: String,
        required: true
    },
    email:{
        type:String,
        required:true
    },
    role: {
        type: String,
        enum: ['DRIVER', 'USER'],
        default: 'USER'
      }
},{timestamp: true});

module.exports = mongoose.model('user' , userSchema);