const express = require('express');
const router = require('./router/userRouts');
const mongoose = require('mongoose');
const rideRoute = require('./router/rideRouts');


const app = express();
app.use(express.json());

app.use('/api/users' , router)
app.use('/api/rides', rideRoute)

mongoose.connect('mongodb://127.0.0.1/Ride_Share_Test_Task')
.then(()=>{
    app.listen(8000 , ()=>{
        console.log('server run on port 8000');
    });
})
.catch((err)=>{
    console.log(err);
})
