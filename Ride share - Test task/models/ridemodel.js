const mongoose = require('mongoose');

const rideSchema = mongoose.Schema({
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    // type:String,
    ref: "user",
    required: true
  },
  start_location: {
    longitude: {
      type: String,
      required: true
    },
    latitude: {
      type: String,
      required: true
    }
  },
  end_location: {
    longitude: {
      type: String,
      required: true
    },
    latitude: {
      type: String,
      required: true
    },
    
  },
  status: {
    type: String,
    enum: ['AVAILABLE', 'RESERVED','COMPLETE'],
    default: 'AVAILABLE'
  }

}, { timestamps: true });

module.exports = mongoose.model('Ride', rideSchema);
