const userModel = require('../models/usermodel');
// const loginModel = require('../models/loginmodel');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const Secret_key = "SOMETHING";
const Joi = require('joi')
const generateToken = require('../utility/token')
const signup = async (req, res) => {
    const { name, email, password, role } = req.body;

    try {
        const test = userValidation(req.body);
        if (test.error) {
            res.status(400).send(test.error.details[0].message);
            return;
        }

        const existingUser = await userModel.findOne({ email: email });
        if (existingUser) {
            return res.status(400).json({ message: "user already exist" });

        }
        const hashedPassword = await bcrypt.hash(password, 10);
        const result = await userModel.create({
            ...req.body,
            password: hashedPassword
        });



        // const token = jwt.sign({email:result.email , id : result._id},Secret_key)
        const token = generateToken({ id: result._id, email: result.email, role: result.role });
        res.status(201).json({ user: result, token: token });
    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: "something goes wrong"
        });
    }


}

const login = async (req, res) => {
    const { email, password } = req.body;
    try {

        const existingUser = await userModel.findOne({ email: email });
        if (!existingUser) {
            return res.status(404).json({ message: "user not found" });

        }
        const passwordMatch = await bcrypt.compare(password, existingUser.password);
        if (!passwordMatch) return res.status(400).json({ message: "invalid Credentials" });

        // const token = jwt.sign({email:existingUser.email , id: existingUser._id}, Secret_key)
        const token = generateToken({ id: existingUser._id, email: existingUser.email });
        console.log(token);
        res.status(200).json({ msg: "login successfully", user: existingUser, token: token });
    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: "something goes wrong"
        });
    }

}

const getProfile = async (req, res) => {
    try {
        const id = req_body.userId;
        const profile = await userModel.findById(id);
        res.status(200).json({ msg: "profile found", data: profile });
    } catch (error) {
        console.log(error);
        res.status(400).json({ message: "something goes wrong" })
    }
}

const updateProfile = async (req, res) => {

    const { username, email } = req.body;
    // console.log(email);
    const user_id = req_body.userId;
    // console.log(user_id);

    try {
        const newUser = await userModel.findByIdAndUpdate(user_id, { username: username, email: email }, { new: true });
        res.status(200).json(newUser);
    } catch (error) {
        console.log(error);
        res.status(500).json({ message: "something goes wrong with server" });
    }
}

const changePassword = async (req, res) => {
    const id = req_body.userId;
    const dbPassword = await userModel.findById(id);
    const { current_password, new_password } = req.body;
    const passwordMatch = await bcrypt.compare(current_password, dbPassword.password);
    const { error } = passwordValidation({ new_password });
    // if (error) {
    //     console.log(error);
    // }

    
    if (passwordMatch) {
        try {
            if (error) {
                return res.status(400).json({ error: error.details[0].message });
            }
            const pass = await bcrypt.hash(new_password, 10);
            const newpassword = await userModel.findByIdAndUpdate(id, { password: pass }, { new: true });
            res.status(200).json({ msg: "password updated successfully" })
        } catch (error) {
            console.log(error);
            res.status(500).json({ message: "something goes wrong with server" });
        }
    } else {
        res.status(400).json({ msg: "password not match" });
    }



}

function userValidation(user) {
    const schema = Joi.object({
        name: Joi.string().min(3).max(50).required(),
        email: Joi.string().min(6).max(255).required().email(),
        password: Joi.string().min(5).max(10).required(),
        role: Joi.string()
    })
    return schema.validate(user);
}

function passwordValidation(pass1) {
    const passwordSchema = Joi.object({
        new_password: Joi.string()
            .min(5)
            .max(10)
            .required()
            .regex(/^(?=.*[A-Z]).*$/)
            .messages({
                'string.base': 'Password must be a string',
                'string.empty': 'Password is required',
                'string.min': 'Password should have a minimum length of {#limit}',
                'string.pattern.base': 'Password must contain at least one uppercase letter',
                'any.required': 'Password is required',
            }),
    });
    return passwordSchema.validate(pass1);
}

module.exports = { signup, login, userValidation, getProfile, updateProfile, changePassword }