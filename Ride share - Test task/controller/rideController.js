const Ride = require('../models/ridemodel');
const { ObjectId } = require('mongodb');

// Create a new ride
const createRide = async (req, res) => {
    try {
        const role = req_body.role;
        // console.log(role);

        if (role == 'USER') {
            const userId = ObjectId.createFromHexString(req_body.userId);
            const {start_location, end_location } = req.body;
            // console.log(typeof (userId));
           
            // console.log(typeof (userId));
            // console.log(userId);
            // console.log(start_location);
            // console.log(end_location);
            // Create a new ride object
            const ride = new Ride({
                userId,
                start_location,
                end_location
            });

            // Save the ride to the database
            const createdRide = await ride.save();

            res.status(201).json({ message: 'Ride created successfully', ride: createdRide });
        }
        else {
            res.status(400).send("this is not a valid user")
        }
    } catch (error) {
        console.error('Error creating ride:', error);
        res.status(500).json({ message: 'Failed to create ride' });
    }
};


const getHistory = async (req, res) => {
    try {
        const history = await Ride.find({ userId: req_body.userId }).populate("userId");
        res.status(200).json(history);
    } catch (error) {
        console.log(error);
        res.status(400).json({ msg: "something goes wrong" })
    }
}

const availableRide = async (req, res) => {
    try {
        const role = req_body.role;
        if (role == 'DRIVER') {
            const available = await Ride.find({ status: "AVAILABLE" });
            res.status(200).send(available);
        } else {
            res.status(404).send("this is not driver")
        }
    } catch (error) {
        console.log(error);
    }
}

const selectRide = async (req, res) => {
    const role = req_body.role;
    if (role == 'DRIVER') {
        const id = req.params.id;
        const { status } = await Ride.findById(id)
        if (status == 'RESERVED') {
            res.status(400).send("this ride is reserved by some other driver")
        } else if (status == 'COMPLETED') {
            res.status(400).send("this ride is completed")
        } else {
            try {
                const newRide = await Ride.findByIdAndUpdate(id, { status: "RESERVED" }, { new: true });
                res.status(200).json({ msg: "ride is reserved by you", rideDetail: newRide });
            } catch (error) {
                console.log(error);
                res.status(400).send("something goes wrong")
            }

        }
    } else {
        res.status(404).send("this is not driver")
    }
    //  console.log(status);

}

const rideComplete = async (req, res) => {
    const role = req_body.role;
    if (role == 'DRIVER') {
        const id = req.params.id;
        try {
            const completeRide = await Ride.findByIdAndUpdate(id, { status: "COMPLETED" }, { new: true });
            res.status(200).json({ msh: "Ride is completed", rideDetail: completeRide });
        } catch (error) {
            console.log(error);
            res.status(400).json({ msg: "something goes wrong" })
        }
    } else {
        res.status(404).send("this is not driver")
    }
}

module.exports = { createRide, getHistory, availableRide, selectRide, rideComplete }