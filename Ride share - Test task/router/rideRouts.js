const express = require('express');
const auth = require('../middleware/auth');
const {createRide, getHistory, availableRide, selectRide, rideComplete} = require('../controller/rideController');
const rideRoute = express.Router();


rideRoute.post('/request' ,auth, createRide);

rideRoute.get('/history' ,auth, getHistory);
rideRoute.get('/available',auth, availableRide);
rideRoute.patch('/select/:id' ,auth, selectRide);
rideRoute.patch('/complete/:id' ,auth, rideComplete)




module.exports = rideRoute