const express = require('express');
const { signup, login, getProfile, updateProfile, changePassword } = require('../controller/userController');
const auth = require('../middleware/auth');
const router = express.Router();

router.post('/login', login);

router.post('/signup' , signup);

router.get('/profile',auth, getProfile);

router.patch('/profile' ,auth, updateProfile);

router.post('/change-password' ,auth, changePassword);



module.exports= router